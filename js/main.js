jQuery(function($){

  $('input[placeholder], textarea[placeholder]').placeholder();

  // add-open-class
  $('.menu-icon').click(function(){
   if($(this).parent().is('.menu-open')){
     $(this).parent().removeClass('menu-open');
     $('body').removeClass('menu-open-wrapper-page');
   }else{
     $(this).parent().addClass('menu-open');
     $('body').addClass('menu-open-wrapper-page');
   }
  });
  $('.general-select').styler();

});//end ready

//Plugin placeholder
(function(b,f,i){function l(){b(this).find(c).each(j)}function m(a){for(var a=a.attributes,b={},c=/^jQuery\d+/,e=0;e<a.length;e++)if(a[e].specified&&!c.test(a[e].name))b[a[e].name]=a[e].value;return b}function j(){var a=b(this),d;a.is(":password")||(a.data("password")?(d=a.next().show().focus(),b("label[for="+a.attr("id")+"]").attr("for",d.attr("id")),a.remove()):a.realVal()==a.attr("placeholder")&&(a.val(""),a.removeClass("placeholder")))}function k(){var a=b(this),d,c;placeholder=a.attr("placeholder");
b.trim(a.val()).length>0||(a.is(":password")?(c=a.attr("id")+"-clone",d=b("<input/>").attr(b.extend(m(this),{type:"text",value:placeholder,"data-password":1,id:c})).addClass("placeholder"),a.before(d).hide(),b("label[for="+a.attr("id")+"]").attr("for",c)):(a.val(placeholder),a.addClass("placeholder")))}var g="placeholder"in f.createElement("input"),h="placeholder"in f.createElement("textarea"),c=":input[placeholder]";b.placeholder={input:g,textarea:h};!i&&g&&h?b.fn.placeholder=function(){}:(!i&&g&&
!h&&(c="textarea[placeholder]"),b.fn.realVal=b.fn.val,b.fn.val=function(){var a=b(this),d;if(arguments.length>0)return a.realVal.apply(this,arguments);d=a.realVal();a=a.attr("placeholder");return d==a?"":d},b.fn.placeholder=function(){this.filter(c).each(k);return this},b(function(a){var b=a(f);b.on("submit","form",l);b.on("focus",c,j);b.on("blur",c,k);a(c).placeholder()}))})(jQuery,document,window.debug);




$(document).ready(function() {

  $('.product-information-links').on('click', 'div:not(.product-information-link-active)', function() {
    $(this)
      .addClass('product-information-link-active').siblings().removeClass('product-information-link-active')
      .closest('.product-information-container').find('.product-information-tab').removeClass('product-information-tab-active').eq($(this).index()).addClass('product-information-tab-active');
  });

  $("#mainpage-slider").bxSlider({
    minSlides: 3,
    maxSlides: 3,
    slideWidth: 380
  });


});
